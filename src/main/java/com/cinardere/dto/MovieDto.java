package com.cinardere.dto;

import lombok.Data;

/**
 *
 */
@Data
public class MovieDto {

    private String title;
    private Genre genre;
    private String director;
}
