package com.cinardere.dto;

/***
 *
 */
public enum Genre {

    ACTION, THRILLER, DRAMA, SCIENCE_FICTION;
}
